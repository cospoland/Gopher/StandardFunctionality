package StandardFunctionality

import (
	"gitlab.com/cospoland/Gopher/GopherDK"
)

type ModuleContext struct {
	Module GopherDK.Module
}

func (context ModuleContext) Start() bool {
	return false
}

func Ping(Context GopherDK.MessageContext) {
	Context.Provider.Methods.Send(Context, "Pong.")
}

func New() GopherDK.Module {
	mod := GopherDK.Module{}
	mod.Name = "Standard"
	mod.Version = "v-git::master"
	mod.Author = "Pyy"
	mod.Methods = ModuleContext{Module: mod}
	mod.Commands = map[string]interface{}{
		"ping": Ping,
	}
	return mod
}
